﻿using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    [SerializeField] private Color m_Color;

    public Color Color { get => m_Color; set => m_Color = value; }

    private void OnDrawGizmos()
    {
        Gizmos.color = m_Color;
        Gizmos.DrawSphere(transform.position, 0.5f);
    }
}
