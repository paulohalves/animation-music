﻿using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private PlayerDamageHandler m_PlayerDamageHandler = default;
    [SerializeField] private PlayerHUDManager m_PlayerHUDManager = default;

    public PlayerDamageHandler PlayerDamageHandler { get => m_PlayerDamageHandler; }

    private void Awake()
    {
        PlayerDamageHandler.OnUpdateHealth += HandleOnUpdateHealth;
    }

    private void HandleOnUpdateHealth(float healthPercentage)
    {
        m_PlayerHUDManager.SetHealth(healthPercentage);
    }
}
