﻿using UnityEngine;

public class PlayerDamageHandler : DamageHandler
{
    [SerializeField] private float _recoverySpeed = 20.0f;
    [SerializeField] private float _recoveryDelay = 3.0f;

    private float _lastTimeDamage;

    protected override void Awake()
    {
        base.Awake();
        OnDamage += HandleOnDamage;
    }

    private void HandleOnDamage(float damage, Vector3 hitPosition, Vector3 hitDirection)
    {
        _lastTimeDamage = Time.time;
    }

    private void Update()
    {
        if ((Time.time > _lastTimeDamage + _recoveryDelay) && (CurrentHealth < _maxHealth))
        {
            Heal(_recoverySpeed * Time.deltaTime);
        }
    }
}
