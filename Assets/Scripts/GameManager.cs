﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [System.Serializable]
    public class LightGroup
    {
        [SerializeField] public string ColorHash = default;
        [SerializeField] public List<Light> Lights = new List<Light>();
    }

    [SerializeField] private SpectrumTest _spectrumManager = default;
    [SerializeField] private List<LightGroup> _lightGroups = new List<LightGroup>();
    [SerializeField] private Light _spotLight = default;
    [SerializeField] private float _beatLightSpeed = 4.0f;
    [SerializeField] private float _maxIntensity = 1.0f;
    [Header("Player")]
    [SerializeField] private Player _player = default;
    [Header("Zombies")]
    [SerializeField] private Object _zombiePrefab = default;
    [SerializeField] private SpawnPoint[] _spawnPoints = default;
    [SerializeField] private int _maxZombies = 50;

    private int _zombies;
    private float _beatIntensity = 0;
    private float _zombieSpeedMultiplier = 1;
    private bool _gameStarted = false;

    private readonly Dictionary<string, List<SpawnPoint>> _spawnPointsDictionary = new Dictionary<string, List<SpawnPoint>>();
    private readonly Dictionary<string, LightGroup> _lightsDictionary = new Dictionary<string, LightGroup>();
    private readonly Dictionary<string, float> _colorsIntensity = new Dictionary<string, float>();

    private void Awake()
    {
        foreach(LightGroup lightGroup in _lightGroups)
        {
            _lightsDictionary.Add(lightGroup.ColorHash, lightGroup);
        }

        foreach (SpawnPoint spawnPoint in _spawnPoints)
        {
            string color = ColorUtility.ToHtmlStringRGB(spawnPoint.Color);
            if (!_spawnPointsDictionary.ContainsKey(color))
            {
                _spawnPointsDictionary.Add(color, new List<SpawnPoint>());
            }

            _spawnPointsDictionary[color].Add(spawnPoint);
        }


        _spectrumManager.OnChangeSpectrum += HandleOnChangeSpectrum;
        _spectrumManager.OnBeat += HandleOnBeat;
        _player.PlayerDamageHandler.OnDie += HandleOnPlayerDie;
    }

    private void HandleOnPlayerDie()
    {
        SceneManager.LoadScene("Menu");
    }

    private void Start()
    {
        StartCoroutine(PlayAudio(PlayerPrefs.GetString("song")));
    }

    private void Update()
    {
        if (_gameStarted)
        {
            if (_spectrumManager.AudioSource.isPlaying)
            {
                _spotLight.intensity = Mathf.Max(0, _spotLight.intensity - _beatLightSpeed * Time.deltaTime);
                _zombieSpeedMultiplier = 1 + (_spectrumManager.AudioSource.time / _spectrumManager.AudioSource.clip.length);
            }
            else
            {
                SceneManager.LoadScene("Menu");
            }
        }
    }

    private IEnumerator PlayAudio(string file)
    {
        UnityWebRequest www = new UnityWebRequest("file://" + file);
        www.downloadHandler = new DownloadHandlerAudioClip("file://" + file, AudioType.UNKNOWN);
        yield return www.SendWebRequest();

        while (!www.isDone)
        {
            yield return null;
        }

        while (!www.downloadHandler.isDone)
        {
            yield return null;
        }

        _spectrumManager.AudioSource.clip = DownloadHandlerAudioClip.GetContent(www);
        _spectrumManager.AudioSource.Play();
        _gameStarted = true;
    }

    private void HandleOnBeat()
    {
        if (_zombies >= _maxZombies)
        {
            return;
        }

        string color = GetRandomSpectrumColor();

        List<SpawnPoint> _sps = _spawnPointsDictionary[color];
        Transform spawnPoint = _sps[Random.Range(0, _sps.Count)].transform;

        GameObject zombieGO = Instantiate(_zombiePrefab, spawnPoint.position, Quaternion.identity) as GameObject;
        Zombie zombie = zombieGO.GetComponent<Zombie>();
        zombie.SetColor(color);
        zombie.SpeedMultiplier = _zombieSpeedMultiplier;
        zombie.Target = _player.transform;
        zombie.DamageHandler.OnDie += HandleOnDie;

        BeatLight(color, _beatIntensity);

        _zombies++;
    }

    private void BeatLight(string color, float intensity)
    {
        Color c = Color.white;
        if (ColorUtility.TryParseHtmlString("#" + color, out c))
        {
            _spotLight.intensity = Mathf.Min(_maxIntensity, _beatIntensity);
            _spotLight.color = c;
        }
    }

    private void HandleOnDie()
    {
        _zombies--;
    }

    private void HandleOnChangeSpectrum(string color, float amplitude)
    {
        if (_lightsDictionary.ContainsKey(color))
        {
            LightGroup lightGroup = _lightsDictionary[color];
            foreach(Light light in lightGroup.Lights)
            {
                light.intensity = amplitude;
            }
        }

        if (_colorsIntensity.ContainsKey(color))
        {
            _colorsIntensity[color] = amplitude;
        }
        else
        {
            _colorsIntensity.Add(color, amplitude);
        }
    }

    private string GetRandomSpectrumColor()
    {
        float total = 0;
        foreach(KeyValuePair<string, float> color in _colorsIntensity)
        {
            total += color.Value;
        }

        float r = Random.Range(0.0f, total);
        total = 0;

        foreach (KeyValuePair<string, float> color in _colorsIntensity)
        {
            total += color.Value;
            if (r < total)
            {
                _beatIntensity = color.Value;
                return color.Key;
            }
        }

        return null;
    }
}
