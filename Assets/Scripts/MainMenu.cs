﻿using SFB;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public delegate void FilesHandler(string[] filePaths);

    [SerializeField] private Button _loadFileButton = default;
    [SerializeField] private Button _startGame = default;
    [SerializeField] private Text _loadedFileText = default;

    private string m_LoadedFile;

    public string LoadedFile
    {
        get => m_LoadedFile;
        set
        {
            m_LoadedFile = value;
            _loadedFileText.text = m_LoadedFile;
        }
    }

    private void Awake()
    {
        _loadFileButton.onClick.AddListener(OpenLoadFile);
        _startGame.onClick.AddListener(StartGame);
    }

    private void Start()
    {
        LoadedFile = PlayerPrefs.GetString("song");
    }

    private void OpenLoadFile()
    {
        LoadFileAsync((files) => 
        {
            if (files.Length <= 0)
            {
                return;
            }

            PlayerPrefs.SetString("song", files[0]);
            LoadedFile = files[0];
        } );
    }

    private void LoadFileAsync(FilesHandler filesCallback)
    {
        var extensions = new[] {
            new ExtensionFilter("Ogg Vorbis Files", "ogg"),
            new ExtensionFilter("Microsoft Wave", "wav")
        };

        StandaloneFileBrowser.OpenFilePanelAsync("Open File", "", extensions, false, filesCallback.Invoke);
    }

    private void StartGame()
    {
        if (!string.IsNullOrEmpty(LoadedFile))
        {
            SceneManager.LoadScene("Arena");
        }
    }
}
