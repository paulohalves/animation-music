﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SpectrumTest : MonoBehaviour
{
    public delegate void BeatHandler();
    public delegate void SpectrumHandler(string color, float amplitude);
    public event BeatHandler OnBeat;
    public event SpectrumHandler OnChangeSpectrum;

    [Header("Beat")]
    [SerializeField] private int _minimumBass = 40;
    [SerializeField] private int _maximumBass = 200;
    [SerializeField] private float _deltaAmplitude = 0.3f;
    [SerializeField] private float _minimumAmplitude = 0.3f;
    [SerializeField] private float _maximumAmplitude = 1;
    [SerializeField] private int _minimumBeatPerSecond = 150;
    [SerializeField] private float _settleSpeed = 0;

    [Header("SpectrumAreas")]
    [SerializeField, Range(0.0f, 1.0f)] private float m_SmoothBlend = 0;
    [SerializeField, Min(1.0f)] private float m_AmplitudeMultiplier = 1.0f;
    [SerializeField] private SpectrumArea[] _spectrumAreas = default;

    private float _totalBassAmplitude;
    private float _lastBassAmplitude;
    private float _lastBeatTime;
    private float _totalAmplitude;

    private bool _beat;

    private AudioSource m_AudioSource;

    public AudioSource AudioSource
    {
        get
        {
            if (m_AudioSource == null)
            {
                m_AudioSource = GetComponent<AudioSource>();
            }

            return m_AudioSource;
        }
    }

    private float MinimumBeatDelay
    {
        get
        {
            return 60f / _minimumBeatPerSecond;
        }
    }

    void Update()
    {
        float[] spectrum = new float[1024];

        AudioListener.GetSpectrumData(spectrum, 0, FFTWindow.Rectangular);

        _lastBassAmplitude = _totalBassAmplitude;
        _totalBassAmplitude = 0;

        bool beat = false;

        for (int a = 1; a < (_spectrumAreas.Length - 1); a++)
        {
            _spectrumAreas[a].CurrentAmplitude = 0;
        }

        for (int i = 1; i < spectrum.Length - 1; i++)
        {
            float amplitude = spectrum[i];

            _totalAmplitude = Mathf.Max(_totalAmplitude, amplitude);

            for (int a = 1; a < (_spectrumAreas.Length - 1); a++)
            {
                if (i >= _spectrumAreas[a].MinimumFrequency && i <= _spectrumAreas[a].MaximumFrequency)
                {
                    _spectrumAreas[a].CurrentAmplitude = Mathf.Max(_spectrumAreas[a].CurrentAmplitude, amplitude);

                    float t = ((float)(i - _spectrumAreas[a].MinimumFrequency) / (float)(_spectrumAreas[a].MaximumFrequency - _spectrumAreas[a].MinimumFrequency)); //((t-tA)/(tB-tA))
                    Color c = _spectrumAreas[a].AreaColor;

                    if (t < 0.5f)
                    {
                        c = Color.Lerp(_spectrumAreas[a].AreaColor, _spectrumAreas[a - 1].AreaColor, 1 - ((t + 0.5f) * m_SmoothBlend));
                    }
                    else
                    {
                        c = Color.Lerp(_spectrumAreas[a].AreaColor, _spectrumAreas[a + 1].AreaColor, (t - 0.5f) * m_SmoothBlend);
                    }

                    OnChangeSpectrum?.Invoke(ColorUtility.ToHtmlStringRGB(_spectrumAreas[a].AreaColor), _spectrumAreas[a].CurrentAmplitude * m_AmplitudeMultiplier * a);
                    break;
                }
            }

            if (i >= _minimumBass && i <= _maximumBass)
            {
                _totalBassAmplitude = Mathf.Max(_totalBassAmplitude, amplitude);

                if (amplitude >= _maximumAmplitude)
                {
                    if (Time.time >= _lastBeatTime + MinimumBeatDelay)
                    {
                        beat = true;
                        if (!_beat)
                        {
                            _lastBeatTime = Time.time;
                            _beat = true;
                            OnBeat?.Invoke();
                        }
                    }
                }
            }
        }

        _maximumAmplitude = Mathf.Max(_maximumAmplitude, _totalBassAmplitude - _deltaAmplitude);
        _maximumAmplitude = Mathf.Max(_minimumAmplitude, _maximumAmplitude - (_settleSpeed * Time.deltaTime));

        if (beat == false)
        {
            _beat = false;
        }
    }
}