﻿using UnityEngine;

public class DamageHandler : MonoBehaviour
{
    public delegate void DeathHandler();
    public delegate void DamagedHandler(float damage, Vector3 hitPosition, Vector3 hitDirection);
    public delegate void HealthHandler(float healthPercentage);
    public event DeathHandler OnDie;
    public event DamagedHandler OnDamage;
    public event HealthHandler OnUpdateHealth;

    [SerializeField] protected float _maxHealth = 100.0f;

    private float m_CurrentHealth;

    public float CurrentHealth
    {
        get => m_CurrentHealth;
        private set
        {
            m_CurrentHealth = value;
            OnUpdateHealth?.Invoke(m_CurrentHealth/_maxHealth);
        }
    }

    protected virtual void Awake()
    {
        CurrentHealth = _maxHealth;
    }

    public void DealDamage(float damage, Vector3 hitPosition, Vector3 hitDirection)
    {
        CurrentHealth = Mathf.Clamp(CurrentHealth - damage, 0, _maxHealth);

        OnDamage?.Invoke(damage, hitPosition, hitDirection);

        if (CurrentHealth == 0)
        {
            OnDie?.Invoke();
        }
    }

    public void Heal(float health)
    {
        CurrentHealth = Mathf.Clamp(CurrentHealth + health, 0, _maxHealth);
    }
}
