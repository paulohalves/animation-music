﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PlayerAnimationHandler : MonoBehaviour
{
    private readonly int HashForward = Animator.StringToHash("Forward");
    private readonly int HashStrafe = Animator.StringToHash("Strafe");

    private Animator m_Animator;

    public Animator Animator
    {
        get
        {
            if (m_Animator == null)
            {
                m_Animator = GetComponent<Animator>();
            }

            return m_Animator;
        }
    }

    public void SetForward(float forward)
    {
        Animator.SetFloat(HashForward, forward);
    }

    public void SetStrafe(float strafe)
    {
        Animator.SetFloat(HashStrafe, strafe);
    }

}
