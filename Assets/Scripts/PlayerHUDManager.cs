﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerHUDManager : MonoBehaviour
{
    [SerializeField] private Image _damageInterface = default;

    public void SetHealth(float healthPercentage)
    {
        Color c = _damageInterface.color;
        c.a = 1 - healthPercentage;
        _damageInterface.color = c;
    }
}
