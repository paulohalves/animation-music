﻿using UnityEngine;

public class ZombieAnimationEventHandler : MonoBehaviour
{
    public delegate void AttackHandler();
    public event AttackHandler OnAttack;

    private void Attack()
    {
        OnAttack?.Invoke();
    }
}
