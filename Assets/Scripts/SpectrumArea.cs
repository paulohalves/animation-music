﻿using UnityEngine;

[System.Serializable]
public class SpectrumArea
{
    [SerializeField] private Color m_AreaColor = Color.black;
    [SerializeField] private int m_MinimumFrequency = 0;
    [SerializeField] private int m_MaximumFrequency = 0;

    private float _currentAmplitude;

    public Color AreaColor { get => m_AreaColor; private set => m_AreaColor = value; }
    public float CurrentAmplitude { get => _currentAmplitude; set => _currentAmplitude = value; }
    public int MinimumFrequency { get => m_MinimumFrequency; private set => m_MinimumFrequency = value; }
    public int MaximumFrequency { get => m_MaximumFrequency; private set => m_MaximumFrequency = value; }
}
