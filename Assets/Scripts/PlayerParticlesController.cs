﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerParticlesController : MonoBehaviour
{
    [SerializeField] private ParticleSystem _muzzleFlash = default;
    [SerializeField] private Object _hitImpact = default;
    [SerializeField] private int _hitImpactPoolSize = 20;

    private int _hitImpactPoolIndex = 0;

    private readonly List<ParticleSystem> _hitImpactPool = new List<ParticleSystem>();

    private void Awake()
    {
        for(int i = 0; i < _hitImpactPoolSize; i++)
        {
            GameObject go = Instantiate(_hitImpact) as GameObject;
            go.SetActive(false);
            _hitImpactPool.Add(go.GetComponent<ParticleSystem>());
        }
    }

    public void Shoot()
    {
        _muzzleFlash.Play();
    }

    public void Hit(Vector3 position, Vector3 normal)
    {
        _hitImpactPool[_hitImpactPoolIndex].gameObject.SetActive(true);
        _hitImpactPool[_hitImpactPoolIndex].gameObject.transform.position = position;
        _hitImpactPool[_hitImpactPoolIndex].gameObject.transform.rotation = Quaternion.LookRotation(normal);
        _hitImpactPool[_hitImpactPoolIndex].Play();

        _hitImpactPoolIndex = (_hitImpactPoolIndex + 1) % _hitImpactPoolSize;
    }
}
