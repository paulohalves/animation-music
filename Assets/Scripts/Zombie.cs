﻿using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(CapsuleCollider))]
public class Zombie : MonoBehaviour
{
    [SerializeField] private ParticleSystem m_DamageParticle = default;
    [SerializeField] private ParticleSystem m_SpawnParticle = default;
    [SerializeField] private ZombieAnimationHandler m_ZombieAnimationHandler = default;
    [SerializeField] private ZombieAnimationEventHandler m_ZombieAnimationEventHandler = default;
    [SerializeField] private DamageHandler m_DamageHandler = default;
    [SerializeField] private Renderer m_Renderer = default;
    [SerializeField] private NavMeshAgent m_NavMeshAgent = default;
    [SerializeField] private Transform m_Target = default;
    [Header("Movement")]
    [SerializeField] private float m_MovementSpeed = 2.0f;
    [SerializeField] private float m_SpeedMultiplier = 1.0f;
    [Header("Attack")]
    [SerializeField] private LayerMask _attackLayer = default;
    [SerializeField] private float m_AttackTime = 1.3f;
    [SerializeField] private float _damage = 10.0f;

    private CapsuleCollider m_Collider;
    private float _lastAttackTime;

    public CapsuleCollider Collider
    {
        get
        {
            if (m_Collider == null)
            {
                m_Collider = GetComponent<CapsuleCollider>();
            }

            return m_Collider;
        }
    }

    public DamageHandler DamageHandler { get => m_DamageHandler; private set => m_DamageHandler = value; }
    public Transform Target { get => m_Target; set => m_Target = value; }
    public float SpeedMultiplier
    {
        get => m_SpeedMultiplier;
        set
        {
            m_SpeedMultiplier = value;
            m_NavMeshAgent.speed = m_MovementSpeed * m_SpeedMultiplier;
        }
    }

    private void Awake()
    {
        DamageHandler.OnDamage += HandleOnDamage;
        DamageHandler.OnDie += HandleOnDie;
        m_ZombieAnimationEventHandler.OnAttack += HandleOnAttack;
        m_NavMeshAgent.speed = m_MovementSpeed * m_SpeedMultiplier;
    }


    private void Start()
    {
        m_SpawnParticle.transform.SetParent(null);
    }

    private void Update()
    {
        m_NavMeshAgent.SetDestination(Target.position);

        if (Vector3.Distance(transform.position, Target.position) <= m_NavMeshAgent.stoppingDistance)
        {
            Attack();
        }
        else
        {
            m_ZombieAnimationHandler.SetAttacking(false);
            m_ZombieAnimationHandler.SetMoving(m_MovementSpeed * m_SpeedMultiplier);
        }
    }

    private void HandleOnAttack()
    {
        Vector3 point = Collider.bounds.center + transform.forward * Collider.radius * 2.0f;

        Collider[] colliders = Physics.OverlapBox(point, Collider.bounds.extents / 2.0f, transform.rotation, _attackLayer);
        foreach (Collider collider in colliders)
        {
            DamageHandler damageHandler = collider.GetComponent<DamageHandler>();

            if (damageHandler)
            {
                damageHandler.DealDamage(_damage, point, transform.forward);
            }
        }

        m_NavMeshAgent.isStopped = false;
    }

    private void HandleOnDamage(float damage, Vector3 hitPosition, Vector3 hitDirection)
    {
        m_DamageParticle.gameObject.SetActive(true);
        m_DamageParticle.gameObject.transform.position = hitPosition;
        m_DamageParticle.gameObject.transform.rotation = Quaternion.LookRotation(hitDirection);
        m_DamageParticle.Play();
    }

    private void HandleOnDie()
    {
        Destroy(gameObject);
    }

    private void Attack()
    {
        if (Time.time < (_lastAttackTime + m_AttackTime))
        {
            m_ZombieAnimationHandler.SetAttacking(false);
            return;
        }
        m_NavMeshAgent.isStopped = true;
        m_ZombieAnimationHandler.SetMoving(0);
        m_ZombieAnimationHandler.SetAttacking(true);
        _lastAttackTime = Time.time;
    }

    public void SetColor(string color)
    {
        Color c = Color.white;
        if (ColorUtility.TryParseHtmlString("#" + color, out c))
        {
            m_Renderer.material.SetColor("_EmissionColor", c);
            var main = m_SpawnParticle.main;
            main.startColor = c;
        }
    }
}
