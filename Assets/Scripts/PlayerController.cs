﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private CharacterController m_CharacterController = default;
    [SerializeField] private PlayerAnimationHandler m_PlayerAnimationHandler = default;
    [SerializeField] private PlayerParticlesController m_PlayerParticlesController = default;
    [SerializeField] private Transform m_CameraTarget = default;
    [Header("Movement")]
    [SerializeField] private float _movementSpeed = 5.0f;
    [SerializeField] private float _maximumAimingDistance = 5.0f;
    [Header("Weapon")]
    [SerializeField] private int _fireRate = 300;
    [SerializeField] private int _damage = 10;

    private Camera _camera;
    private Vector3 _movementInput;
    private Vector3 _mouseWorldPoint;

    private float _lastShotTime;

    private void Awake()
    {
        _camera = Camera.main;
    }

    private void Update()
    {
        _movementInput.x = Input.GetAxis("Horizontal");
        _movementInput.z = Input.GetAxis("Vertical");

        if (Input.GetButton("Fire1"))
        {
            Fire();
        }

        Movement();
        Rotation();
    }

    private void Movement()
    {
        Vector3 move = _movementInput.normalized * _movementSpeed * Time.deltaTime;
        m_CharacterController.Move(_movementInput.normalized * _movementSpeed * Time.deltaTime);

        Vector3 localMove = transform.InverseTransformDirection(_movementInput.normalized);
        m_PlayerAnimationHandler.SetForward(localMove.z);
        m_PlayerAnimationHandler.SetStrafe(localMove.x);
    }

    private void Rotation()
    {
        _mouseWorldPoint = GetMousePoint();
        _mouseWorldPoint.y = transform.position.y;
        Vector3 dir = Vector3.ClampMagnitude(_mouseWorldPoint - transform.position, _maximumAimingDistance);

        if (dir.sqrMagnitude > 0)
        {
            m_CameraTarget.transform.position = transform.position + dir;
            transform.rotation = Quaternion.LookRotation(dir);
        }
    }

    private void Fire()
    {
        if (Time.time < _lastShotTime + (60.0f / _fireRate))
        {
            return;
        }

        RaycastHit hit;
        Vector3 dir = (_mouseWorldPoint - transform.position).normalized;

        Ray ray = new Ray(transform.position + transform.up * m_CharacterController.height / 2.0f, dir);

        if (Physics.Raycast(ray, out hit, 100))
        {
            DamageHandler damageHandler = hit.collider.GetComponent<DamageHandler>();

            if (damageHandler)
            {
                damageHandler.DealDamage(_damage, hit.point, dir);
            }
        }

        m_PlayerParticlesController.Shoot();
        _lastShotTime = Time.time;
    }

    private Vector3 GetMousePoint()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 100))
        {
            return hit.point;
        }

        return Vector3.zero;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(_mouseWorldPoint, 1);
    }
}
