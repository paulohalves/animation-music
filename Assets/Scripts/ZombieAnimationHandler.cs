﻿using UnityEngine;

public class ZombieAnimationHandler : MonoBehaviour
{
    private readonly int HashMoving = Animator.StringToHash("Movement");
    private readonly int HashAttack = Animator.StringToHash("Attacking");

    private Animator m_Animator;

    public Animator Animator
    {
        get
        {
            if (m_Animator == null)
            {
                m_Animator = GetComponent<Animator>();
            }

            return m_Animator;
        }
    }

    public void SetMoving(float moving)
    {
        Animator.SetFloat(HashMoving, moving);
    }

    public void SetAttacking(bool attacking)
    {
        Animator.SetBool(HashAttack, attacking);
    }

}
